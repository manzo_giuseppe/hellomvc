/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.dcmaester.hellomvc;

/**
 *
 * @author beppe
 */
public interface LoginController{
    
    public abstract boolean login(String username, String password);
    public abstract String getError();
    public abstract void logout();
}
