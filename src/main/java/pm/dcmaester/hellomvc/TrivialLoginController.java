/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.dcmaester.hellomvc;

import pm.dcmaester.minimvc.controller.AbstractController;

/**
 *
 * @author beppe
 */
public class TrivialLoginController extends AbstractController implements LoginController {
    private String username, password, lastError;

    @Override
    public boolean login(String username, String password) {
        if(!username.isEmpty() && !password.isEmpty()){
            return true;
        }else{
            lastError = "Username and Password must not be empty";
            return false;
        }
    }

    @Override
    public String getError() {
        return lastError;
    }

    @Override
    public void logout() {
        username = null;
    }

    @Override
    public void dispose() {
        super.dispose(); //To change body of generated methods, choose Tools | Templates.
        username = null;
        password = null;
        lastError = null;
    }
    
}
